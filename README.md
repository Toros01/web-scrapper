## Name
Web scraper with a monitor comparison site

## Description
This project can parse, scrape and extract data from mulitple e commerce websites based upon the products descrition, price and availability. These tasks are set up to execute autonomously at timed intervals This project can also display this scraped data to users through a website that allows for the product data to be interacted with and for the user to search through the data based upon product name/description. 
Features:
1. Navigate through cookies pop ups
2. Parse, scrape and extract selective data
3. Parse, scrape and extract autonomously 
4. Search bar for searching through database of scraped data 

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

Install the list of packages with the command "pip install package_name" in the command line. This can either be done in a virtual environemnt or not. 
For a virtual environement it is set up from the cmd line with:
1. Navigate to the directroy of the project: "cd c:\directory_name"
2. Create the venv: "python -m venv venvName" 
3. Activate venv: "venvName\Scripts\activate" Once activated the name of the venv will be shown at the start of the directory.
4. Now install the packages into the venv:
Package_name list:
1. beautifulsoup4
2. flask
3. jinja2
4. lxml
5. pymongo
6. requests
7. selenium
8. urlib3 
9. apscheduler
10. Flask-pymongo

5. To deactivate the venv: "deactivate"

Chrome webdriver setup:
1. Visit the chromedriver downloads website and download the same version as your chrome browser and the platform:
https://chromedriver.chromium.org/downloads 
2. Extract the downloaded zip file and copy the path of the directory it was extraced to. 
3. OPen  "edit the system environmental variables" in windows 
4. In the System properties pane, click the "Environment variables" button at the bottom. 
5. Under the "System variables" find and select the "Path" variable and click "Edit."
6. Click "New" and add the path to the chromedriver. 

System execution:
1. Go to the directory of the project and activate the venv if its not already been activated. Go to section above on how to activate venv. 
2. Start the project by running: "python app.py"
3. The system will execute. Go to the url given to interact with the web pages. 

## Support
te78@student.le.ac.uk

## Authors
Toros Ercan

## Operating systems requirments
Windows: Windows 10 and above.

## GitLab
Software artegacts can be found at: 
https://gitlab.com/Toros01/web-scrapper


