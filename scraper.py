import time
import random
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
from database import insert_product
import logging

logging.basicConfig(level=logging.INFO, filename='logger.log', filemode='w',format='%(name)s - %(levelname)s - %(message)s')

def scrapeRaw(url, websiteName, selectorList):
    try:
        driver = webdriver.Chrome()
        driver.get(url)
        logging.info(f"Accessing URL: {url}")

        if websiteName in ["very.co.uk", "currys.co.uk"]:
            acceptButton = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, "#onetrust-accept-btn-handler"))
            )
            acceptButton.click()
            logging.info("Navigated cookies on Very/Currys.")

        if websiteName == "scan.co.uk":
            acceptButton = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, "button.cookies-accept-all"))
            )
            acceptButton.click()
            logging.info("Navigated cookies on Scan.")
        

        time.sleep(random.uniform(1, 3))  
        html = driver.page_source
        driver.quit()
        

        soup = BeautifulSoup(html, 'lxml')

        product_containers = soup.select(selectorList["Container"])
        
        count=0
        for container in product_containers:
            
            
            name = container.select_one(selectorList["Description"]).get_text(strip=True) if container.select_one(selectorList["Description"]) else "No Name"
            prices = container.select_one(selectorList["SalePrice"]) or container.select_one(selectorList["Price"])
            price = prices.get_text(strip=True) if prices else "No Price"
            stock = "In Stock" if container.select_one(selectorList["Stock"]) or selectorList["Stock"] == "defaultStocked" else "Out of Stock"


            product_details = {
                
                "Website": websiteName,
                "Description": name,
                "Price": price,
                "Stock": stock

            }
            
            insert_product(product_details)
            count +=1

        logging.info(f"Scraped {count} items from {websiteName}")

    except Exception as e:
        logging.error(f"Error during scraping!: {str(e)}", exc_info=True)
    finally:
        driver.quit()
        logging.info("WebDriver has closed.")

selectorList = {
    "very.co.uk":  {
         "Container": "li.product.item",

         "Description": "span.productBrandDesc",
         "Price": "dd.productPrice",
         "SalePrice": "dd.productNowPrice",
         "Stock": "dd.available"
    },
    "currys.co.uk":    {
         "Container": "div.list-page-wrapper.new-list-view",

         "Description": "h2.pdp-grid-product-name",
         "Price": "span.value",
         "SalePrice": "x",
         "Stock": "defaultStocked"
    },
    "scan.co.uk":  {
         "Container": "li.featuredProduct.product",

         "Description": "span.description",
         "Price": "span.price",
         "SalePrice": "x",
         "Stock": "span.in.stock"

    }

}

websiteInfo = [
        ("https://www.very.co.uk/electricals/pc-monitors/e/b/101007.end", "very.co.uk"),
        ("https://www.currys.co.uk/computing/computer-monitors/pc-monitors", "currys.co.uk"),
        ("https://www.scan.co.uk/shop/computer-hardware/monitors/all", "scan.co.uk")
]
def scrape():
    for url, name in websiteInfo:
        try:
            scrapeRaw(url, name, selectorList[name])
            logging.info(f"Successfully scraped {url}")
        except Exception as e:
            logging.error(f"Failed to scrape {url} from {name}", exc_info=True)

# if __name__ == "__main__":
scrape()