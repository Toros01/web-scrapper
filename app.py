from flask import Flask, render_template, request, redirect, url_for
from database import get_all_products, products_collection
from scraper import scrape
from apscheduler.schedulers.background import BackgroundScheduler
import logging

logging.basicConfig(level=logging.INFO, filename='logger.log', filemode='w',format='%(name)s - %(levelname)s - %(message)s')


app = Flask(__name__)



scheduler = BackgroundScheduler()
scheduler.add_job(scrape, 'interval', hours=12) 
try:
    scheduler.start()
    logging.info(f"Scheduler successfully started")
except Exception as e:
    logging.error(f"Scheduler failed to start!: {str(e)}")



@app.route("/", methods=["GET", "POST"])
def home():
    try:
        if request.method == "POST":
            return redirect(url_for("search", query=request.form["search"]))
        else:
            products_list = get_all_products()
            return render_template("home.html", productDetails=products_list)
    except Exception as e:
        logging.error(f"Error at home route: {str(e)}")
        return render_template("error.html")


@app.route("/search")
def search():
    try:
        query = request.args.get("query")
        results = list(products_collection.find({"Description": {"$regex": query, "$options": "i"}}))
        return render_template("results.html", results=results)
    except Exception as e:
        logging.error(f"Error at search route with query {query}!: {str(e)}")
        return render_template("error.html")

@app.route("/error")
def error():
    return render_template("error.html")


try:
    if __name__ == "__main__":
        app.run(debug=True, use_reloader=False)

except (KeyboardInterrupt):
    scheduler.shutdown(wait=False)
    logging.info("Scheduler shut down successfully")

except Exception as e:
    logging.error(f"Unhandled exception!: {str(e)}")
    scheduler.shutdown(wait=False)
