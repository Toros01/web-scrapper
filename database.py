import pymongo
from urllib.parse import quote_plus
import logging

logging.basicConfig(level=logging.INFO, filename='logger.log', filemode='w',format='%(name)s - %(levelname)s - %(message)s')

username = quote_plus('torosercan')
password = quote_plus('zWXbH7G_WcfSH@3')

try:
    
    client = pymongo.MongoClient(f'mongodb+srv://{username}:{password}@cluster0.wqgzonp.mongodb.net/')
    db = client['price_comparison']
    products_collection = db['products']
    logging.info("Connected to MongoDB successfully.")

except pymongo.errors.ConnectionFailure as e:
    logging.error(f"Connection issue!: Failed to connect to MongoDB: {str(e)}")

except Exception as e:
    logging.error(f"An error occurred: {str(e)}")


# result = products_collection.delete_many({}) 
# print(f"Documents deleted: {result.deleted_count}")



def insert_product(product_details):
    try:
        uniqueId = {"Description": product_details["Description"], "Website": product_details["Website"]}
        update = {"$set": product_details}
        products_collection.update_one(uniqueId, update, upsert=True)
        logging.info(f"Product inserted/updated: {product_details['Description']} from {product_details['Website']}")
    except Exception as e:
        logging.error(f"Failed to insert/update product: {str(e)}")


def get_all_products():
    try:
        products = list(products_collection.find({}))
        logging.info("Fetched all products.")
        return products
    except Exception as e:
        logging.error(f"Failed to fetch products: {str(e)}")
        return []
    
    
